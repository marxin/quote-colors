***Quote Colors***

--------

### About this MailExtension Branch

This MailExtension-Branch is work in progress. The primary goal is to get the add-on for Thunderbird 91 release to work as well as possible in summer 2021 as a pure MailExtension.

### Features

* Configure text, border and background colors of quotes in mail and news messages
* Optionally configure main text, link and signature colors
* Optionally use the chosen colors in compose window, too (from up version 3.0a3)
* Provide separate colors for default (light) and dark mode by detecting "@ media (prefers-color-scheme: dark)" (from up version 3.0a5)
* Incorporate optionally collapsing quotes by click (from up version 3.0a6 / options dialogue from up Version 3.0a7)

### Known issues

* It's not really an issue, but you should know, that Quote Colors doesn't provide all features in compose window. It is intended to have only a subset of features in compose window.
* MailExtension-API doesn't support the features to style printed messages. So Quote Colors doesn't work for printed messages, until the API is improved / added in Thunderbird itself.

### Installation

1. [Download Quote Colors from the official Thunderbird add-on page](https://addons.thunderbird.net/addon/quotecolors/)
2. [Installing an Add-on in Thunderbird](https://support.mozilla.org/kb/installing-addon-thunderbird)


### Contribution

You are welcomed to contribute to this project by:
* adding or improving the localizations of AttachmentExtractor Continued via email to me or a post in german Thunderbird forums [Thunderbird Mail DE](https://www.thunderbird-mail.de/forum/board/81-hilfe-und-fehlermeldungen-zu-thunders-add-ons/) or just create an [issue](https://gitlab.com/ThunderbirdMailDE/quote-colors/issues/) here on GitLab
* creating [issues](https://gitlab.com/ThunderbirdMailDE/quote-colors/issues/) about problems
* creating [issues](https://gitlab.com/ThunderbirdMailDE/quote-colors/issues/) about possible improvements


### Coders

* Alexander Ihrig (Maintainer, Update Thunderbird 68 + 78 + 91)
* Intika (Update Thunderbird 60)
* Malte Rücker (Original "Quote Colors" author)
* Qeole (Some parts of the new QuoteColors MailExtension code were taken from "Colored Diffs" (MPL 2.0 license) by Qeole and adapted for QuoteColors.)
* Michael J Gruber (Some parts of the new QuoteColors MailExtension code were taken from "QuoteCollapse" (MPL 1.1 license) by Michael J. Gruber.)

### Translators

* Tomas Brabenec (Czech)
* Jørgen Rasmussen (Danish)
* Alexander Ihrig (German)
* Carlos (Spanish [Spain])
* Olivier (French)
* Szalai Kálmán (Hungarian)
* Pietro Fistetto (Italian)
* Masahiko Imanaka (Japanese)
* Bartosz Piec (Polish)
* Igor Lyubimov (Russian)


### License

[Mozilla Public License version 2.0](https://gitlab.com/ThunderbirdMailDE/quote-colors/LICENSE)
