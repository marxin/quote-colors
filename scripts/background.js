var options = {};
var composeCss = "";

var msgDisplayScriptsPromise;
var composeScriptsPromise;
var composeCssPromise;

var QCGlobals = {
  // border widths in unit "ex" (correspond to "thin", "medium" and "thick")
  aQC_borderwidth: [0.1667, 0.5, 0.8333],
  // border styles
  aQC_borderstyle: ["double", "solid", "dashed", "dotted"],
  // maximum quote levels - NOT used on all arrays so search for "5" in code additionally
  nQC_MAX_LEVELS: 5
}

var ComposeCssObj = {
  aPrefColorsFg: new Array(QCGlobals.nQC_MAX_LEVELS),
  aPrefColorsBg: new Array(QCGlobals.nQC_MAX_LEVELS),

  aPrefDarkColorsFg: new Array(QCGlobals.nQC_MAX_LEVELS),
  aPrefDarkColorsBg: new Array(QCGlobals.nQC_MAX_LEVELS),

  // ########################################################################
  // basic object initialization

  initMain: function() {
    console.debug("[QuoteColors] [background.js]: ComposeCssObj.initMain");

    // get preferences
    ComposeCssObj.getQCPrefs();

    return ComposeCssObj.generateStyleBlock();
  },

  // ########################################################################
  // read Quote Colors preferences
  // returns: void

  getQCPrefs: function() {
    console.debug("[QuoteColors] [background.js]: ComposeCssObj.getQCPrefs");

    // ########################################################################
    // set value from user prefs

    this.bPrefColorText = options.colorText;
    this.bPrefColorBackground = options.colorBackground;

    this.aPrefColorsFg[0] = options.fg_l1;
    this.aPrefColorsBg[0] = options.bg_l1;

    this.aPrefColorsFg[1] = options.fg_l2;
    this.aPrefColorsBg[1] = options.bg_l2;

    this.aPrefColorsFg[2] = options.fg_l3;
    this.aPrefColorsBg[2] = options.bg_l3;

    this.aPrefColorsFg[3] = options.fg_l4;
    this.aPrefColorsBg[3] = options.bg_l4;

    this.aPrefColorsFg[4] = options.fg_l5;
    this.aPrefColorsBg[4] = options.bg_l5;

    this.aPrefDarkColorsFg[0] = options.dm_fg_l1;
    this.aPrefDarkColorsBg[0] = options.dm_bg_l1;

    this.aPrefDarkColorsFg[1] = options.dm_fg_l2;
    this.aPrefDarkColorsBg[1] = options.dm_bg_l2;

    this.aPrefDarkColorsFg[2] = options.dm_fg_l3;
    this.aPrefDarkColorsBg[2] = options.dm_bg_l3;

    this.aPrefDarkColorsFg[3] = options.dm_fg_l4;
    this.aPrefDarkColorsBg[3] = options.dm_bg_l4;

    this.aPrefDarkColorsFg[4] = options.dm_fg_l5;
    this.aPrefDarkColorsBg[4] = options.dm_bg_l5;

    this.nPrefBorderMode = options.borderMode;
    this.sPrefBorderColor = options.borderColor;
    this.sPrefDarkBorderColor = options.dm_borderColor;

    var nIdxBorderWidth = options.borderWidth;
    this.nBorderWidth = QCGlobals.aQC_borderwidth[nIdxBorderWidth];
    var nIdxBorderStyle = options.borderStyle;
    var sBorderStyle = QCGlobals.aQC_borderstyle[nIdxBorderStyle];
    this.bPrefBorderLeftEn = options.borderposition_left;
    this.bPrefBorderRightEn = options.borderposition_right;
    this.sBorderLeftStyle = this.bPrefBorderLeftEn ? sBorderStyle : "none";
    this.sBorderRightStyle = this.bPrefBorderRightEn ? sBorderStyle : "none";
    this.sBorderTopStyle = options.borderposition_top ? sBorderStyle : "none";
    this.sBorderBottomStyle = options.borderposition_bottom ? sBorderStyle : "none";
    this.bPrefCollapseBorders = options.collapseBorders;

    this.bPrefColorHTMLMsg = options.colorHTMLmessages;

    this.bPrefUseCustomMsgColors = options.usermsgcolors;

    this.sPrefMsgTextColor = options.messagetextcolor;
    this.sPrefMsgBgColor = options.messagebgcolor;
    this.sPrefMsgLinkColor = options.messagelinkcolor;
    this.sPrefMsgLinkHoverColor = options.messagelinkhovercolor;
    this.sPrefSigColor = options.signaturecolor;
    this.sPrefSigLinkColor = options.signaturelinkcolor;

    this.sPrefDarkMsgTextColor = options.dm_messagetextcolor;
    this.sPrefDarkMsgBgColor = options.dm_messagebgcolor;
    this.sPrefDarkMsgLinkColor = options.dm_messagelinkcolor;
    this.sPrefDarkMsgLinkHoverColor = options.dm_messagelinkhovercolor;
    this.sPrefDarkSigColor = options.dm_signaturecolor;
    this.sPrefDarkSigLinkColor = options.dm_signaturelinkcolor;

    this.bPrefHideSignatures = options.hidesignatures;
    this.bPrefHideStructDelim = options.hidestructdelimiters;

    this.bPrefEnableQuotecolorsOnCompose = options.enableQuotecolorsOnCompose;
    this.bPrefEnableUsermsgcolorsOnCompose = options.enableUsermsgcolorsOnCompose;

    this.bPrefEnableQuoteCollapse = options.enableQuoteCollapse;
    this.bPrefQuoteCollapseByDefault = options.quoteCollapseByDefault;
  },

  // ########################################################################
  // generate CSS that will be inserted into mail message
  // returns: string

  generateStyleBlock: function() {
    console.debug("[QuoteColors] [background.js]: generateStyleBlock");

    var sLightStyleBlock = '';
    var sDarkStyleBlock = '';

    const sBqSelector = "blockquote[type=cite] ";
    const sBqSelector5 = sBqSelector + sBqSelector + sBqSelector + sBqSelector + sBqSelector;

    ComposeCssObj.bGraphQuotEn = true;

    // Start "prefers-color-scheme: dark" for Darkmode
    sDarkStyleBlock += "@media (prefers-color-scheme: dark) {\n";

    if (this.bPrefEnableQuotecolorsOnCompose) {
      for (var i = 0; i < QCGlobals.nQC_MAX_LEVELS; i++) {
        var sBqSelectorLevel = '';
        for (var j = 0; j <= i; j++) {
          sBqSelectorLevel += sBqSelector;
        }

        sLightStyleBlock += sBqSelectorLevel + ", " + sBqSelector5 + sBqSelectorLevel;
        sDarkStyleBlock += sBqSelectorLevel + ", " + sBqSelector5 + sBqSelectorLevel;
        sLightStyleBlock += "{";
        sDarkStyleBlock += "{";

        // text color
        sLightStyleBlock += "color:" + (this.bPrefColorText ? this.aPrefColorsFg[i] : "inherit") + " !important;";
        sDarkStyleBlock += "color:" + (this.bPrefColorText ? this.aPrefDarkColorsFg[i] : "inherit") + " !important;";

        // background color
        if (this.bPrefColorBackground) {
          sLightStyleBlock += "background-color:" + this.aPrefColorsBg[i] + " !important;";
          sDarkStyleBlock += "background-color:" + this.aPrefDarkColorsBg[i] + " !important;";
        }

        // only add/style borders if graphical quoting is enabled
        if (this.bGraphQuotEn) {
          sLightStyleBlock += "border-color:" + ((this.nPrefBorderMode == false) ? this.aPrefColorsFg[i] : this.sPrefBorderColor) + " !important;";
          sDarkStyleBlock += "border-color:" + ((this.nPrefBorderMode == false) ? this.aPrefDarkColorsFg[i] : this.sPrefDarkBorderColor) + " !important;";
          sLightStyleBlock += "border-width:" + this.nBorderWidth + "ex !important;";
          sDarkStyleBlock += "border-width:" + this.nBorderWidth + "ex !important;";
          sLightStyleBlock += "border-style:" + this.sBorderTopStyle + " " + this.sBorderRightStyle + " " + this.sBorderBottomStyle + " " + this.sBorderLeftStyle + " !important;";
          sDarkStyleBlock += "border-style:" + this.sBorderTopStyle + " " + this.sBorderRightStyle + " " + this.sBorderBottomStyle + " " + this.sBorderLeftStyle + " !important;";

          if (this.bPrefCollapseBorders) {
            if (i > 0) {
              var leftmargin = !this.bPrefBorderLeftEn ? 1.0 : 1.0;
              var rightmargin = !this.bPrefBorderRightEn ? 1.0 : 1.0;
              sLightStyleBlock += "margin-left: -" + leftmargin + "ex;margin-right: -" + rightmargin + "ex;";
              sDarkStyleBlock += "margin-left: -" + leftmargin + "ex;margin-right: -" + rightmargin + "ex;";
            } else {
              sLightStyleBlock += "margin-block: 1ex !important; padding: 0.4ex 1ex !important;";
              sDarkStyleBlock += "margin-block: 1ex !important; padding: 0.4ex 1ex !important;";
            }
          } else {
            sLightStyleBlock += "margin-block: 1ex !important; padding: 0.4ex 1ex !important;";
            sDarkStyleBlock += "margin-block: 1ex !important; padding: 0.4ex 1ex !important;";
          }

        } else {
          // non-graphical quoting
          sLightStyleBlock += "border: none !important;";
          sDarkStyleBlock += "border: none !important;";
          sLightStyleBlock += "padding: 0em !important;";
          sDarkStyleBlock += "padding: 0em !important;";
        }

        sLightStyleBlock += "}\n";
        sDarkStyleBlock += "}\n";
      } // End for loop

      if (this.bPrefCollapseBorders) {
          sLightStyleBlock += "blockquote[type=cite] pre{margin-left: 0em !important; margin-right: 0em !important;}\n";
          sDarkStyleBlock += "blockquote[type=cite] pre{margin-left: 0em !important; margin-right: 0em !important;}\n";
      }
    }

    // set other messages styles (if enabled)
    if (this.bPrefUseCustomMsgColors && this.bPrefEnableUsermsgcolorsOnCompose) {
      // set text and background colors if enabled
      sLightStyleBlock += "body {color: " + ComposeCssObj.sPrefMsgTextColor + "; background: " + ComposeCssObj.sPrefMsgBgColor + ";}\n";
      sDarkStyleBlock += "body {color: " + ComposeCssObj.sPrefDarkMsgTextColor + "; background: " + ComposeCssObj.sPrefDarkMsgBgColor + ";}\n";

      // set link colors if enabled
      sLightStyleBlock += "a:link {color: " + this.sPrefMsgLinkColor + ";}\n";
      sDarkStyleBlock += "a:link {color: " + this.sPrefDarkMsgLinkColor + ";}\n";
      sLightStyleBlock += "a:link:hover {color: " + this.sPrefMsgLinkHoverColor + ";}\n";
      sDarkStyleBlock += "a:link:hover {color: " + this.sPrefDarkMsgLinkHoverColor + ";}\n";

      // set signature colors if enabled
      sLightStyleBlock += ".moz-txt-sig, .moz-signature {color: " + this.sPrefSigColor + ";}\n";
      sDarkStyleBlock += ".moz-txt-sig, .moz-signature {color: " + this.sPrefDarkSigColor + ";}\n";
      sLightStyleBlock += ".moz-txt-sig > a, .moz-signature > a {color: " + this.sPrefSigLinkColor + ";}\n";
      sDarkStyleBlock += ".moz-txt-sig > a, .moz-signature > a {color: " + this.sPrefDarkSigLinkColor + ";}\n";
    }

    // Close @media for Darkmode
    sDarkStyleBlock += "}\n";

    var LightPlusDarkStyleBlock = sLightStyleBlock + sDarkStyleBlock;
    return LightPlusDarkStyleBlock;
  },
};

function reloadOption(id) {
  return messenger.storage.local.get(id).then((res) => {
    if (res[id] != undefined)
      options[id] = res[id];
    else
      options[id] = DefaultOptions[id];
  }, defaultError);
}

async function reloadAllOptions() {
  await reloadOption("colorText");
  await reloadOption("colorBackground");

  await reloadOption("fg_l1");
  await reloadOption("fg_l2");
  await reloadOption("fg_l3");
  await reloadOption("fg_l4");
  await reloadOption("fg_l5");
  await reloadOption("bg_l1");
  await reloadOption("bg_l2");
  await reloadOption("bg_l3");
  await reloadOption("bg_l4");
  await reloadOption("bg_l5");

  await reloadOption("dm_fg_l1");
  await reloadOption("dm_fg_l2");
  await reloadOption("dm_fg_l3");
  await reloadOption("dm_fg_l4");
  await reloadOption("dm_fg_l5");
  await reloadOption("dm_bg_l1");
  await reloadOption("dm_bg_l2");
  await reloadOption("dm_bg_l3");
  await reloadOption("dm_bg_l4");
  await reloadOption("dm_bg_l5");

  await reloadOption("borderMode");
  await reloadOption("borderColor");
  await reloadOption("dm_borderColor");

  await reloadOption("borderStyle");
  await reloadOption("borderWidth");
  await reloadOption("borderposition_bottom");
  await reloadOption("borderposition_left");
  await reloadOption("borderposition_right");
  await reloadOption("borderposition_top");
  await reloadOption("collapseBorders");

  await reloadOption("colorHTMLmessages");

  await reloadOption("usermsgcolors");

  await reloadOption("messagetextcolor");
  await reloadOption("messagebgcolor");
  await reloadOption("messagelinkcolor");
  await reloadOption("messagelinkhovercolor");
  await reloadOption("signaturecolor");
  await reloadOption("signaturelinkcolor");

  await reloadOption("dm_messagetextcolor");
  await reloadOption("dm_messagebgcolor");
  await reloadOption("dm_messagelinkcolor");
  await reloadOption("dm_messagelinkhovercolor");
  await reloadOption("dm_signaturecolor");
  await reloadOption("dm_signaturelinkcolor");

  await reloadOption("hidesignatures");
  await reloadOption("hidestructdelimiters");

  await reloadOption("enableQuotecolorsOnCompose");
  await reloadOption("enableUsermsgcolorsOnCompose");

  await reloadOption("enableQuoteCollapse");
  await reloadOption("quoteCollapseByDefault");
}

function registerCss() {
  console.debug("[QuoteColors] [registerCss]: composeCss: \n" + composeCss);
  if (!composeCss)
    composeCss = "";
  composeCssPromise = messenger.composeScripts.register({
    css: [{
      code: composeCss,
    }],
  });
}

function registerScripts() {
  let msgDisplayContentScripts = {
    js: [{
        code: "var options = " + JSON.stringify(options) + ";",
      },
      {
        code: "var QCGlobals = " + JSON.stringify(QCGlobals) + ";",
      },
      {
        file: "/scripts/quotecolors_msgDisplay.js",
      },
      {
        file: "/scripts/quotecollapse_msgDisplay.js",
      }
    ]
  };
  msgDisplayScriptsPromise = messenger.messageDisplayScripts.register(
    msgDisplayContentScripts);
}

async function unregisterCss() {
  await composeCssPromise.then(css => css.unregister());
}

async function unregisterScripts() {
  await msgDisplayScriptsPromise.then(script => script.unregister());
}

async function reset() {
  await unregisterCss();
  await unregisterScripts();
  await init();
}

async function init() {
  // await reloadAllOptions is necessary before composeCss = ...
  await reloadAllOptions();
  composeCss = ComposeCssObj.initMain();
  reloadAllOptions().then(registerCss).then(registerScripts);
}

messenger.storage.onChanged.addListener(reset);
init();