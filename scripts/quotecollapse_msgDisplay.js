var QuoteCollapse = {
  oMsgBody: null,

  _onLoad: function() {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_onLoad]");

    QuoteCollapse._getQCPrefs();
    QuoteCollapse._generateStyleBlock();
  },

  _generateStyleBlock: function () {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_generateStyleBlock]");
    
    if (this.bPrefEnableQuoteCollapse) {
      this.oMsgBody = document.body;
      if (!this.oMsgBody.getElementsByTagName("blockquote").item(0))
        return; // nothing to be done

      console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_onLoad]: oMsgBody is existing");
      this.oMsgBody.addEventListener("click", QuoteCollapse._onClick, false);

      var sStyleContent = '\
        blockquote[type="cite"] {\n\
          background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAB3RJTUUH1AsREDArtdKZsQAAAAlwSFlzAABOIAAATiABFn2Z3gAAAARnQU1BAACxjwv8YQUAAAC7SURBVHjabZBPCoJAFMbfG1wFQW2SFt2pW1RnyCN4AC/QqmNItBfaS2BYCwvBCdSZrxlzLKLf6vHx/RmGg/CwJNIRMfv0A4hyBq0oCOP8fCnh0BpoFCBb4JQ+sA3jTLBgfzEfv5MmKgSTJ4hqBZpORmQWZmKoNgajD8hnS5Vsu9v7bnAwcx/Ex2QbnGAN7nZ0psZoVY1uwnFMCpL9nIDGNc3K4Q2uxRqKu7QTN9OE9W6fRPzvn4Bckdq8AE3sc/7yC/yTAAAAAElFTkSuQmCC");\n\
          background-repeat: no-repeat;\n\
          background-position: top left;\n\
          max-height: 1em;\n\
          padding: 1ex !important;\n\
          overflow: -moz-hidden-unscrollable;\n\
        }\n\
        \n\
        blockquote[type="cite"][qctoggled="true"] {\n\
          background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAJCAYAAADgkQYQAAAAB3RJTUUH1AsREDAbkwupHQAAAAlwSFlzAABOIAAATiABFn2Z3gAAAARnQU1BAACxjwv8YQUAAACpSURBVHjaY6zpORrAwPBvBgMjozgDGvjPwPCC8T9DOgtIgZ62rLggPycDOzszAwcHDLMwfPj4TWLjtuvTWUAmYFMAohX4BRiA8mJMIGOxKeAAirGzgaUZWEAESNBMW5ABw03//0MUMUIVwQSwAaCbGMBWPH73C24FGxMjAyvQJiaoIqb///6//PTpO1YFj559Atn5Cuim/xnrt16bwYgtnP7/f/GX4W8mAH/oMd/d5kSRAAAAAElFTkSuQmCC");\n\
          max-height: none;\n\
          overflow: visible;\n\
        }\n\
        ';

      var qrStyle = document.createElement('style');
      qrStyle.classList.add('quotecollapse_msgDisplay');
      //  qrStyle.media = 'screen';
      qrStyle.textContent = sStyleContent;
      if (document.head) {
        document.head.append(qrStyle);
      } else {
        var root = document.documentElement;
        root.append(qrStyle);
        var observer = new MutationObserver(() => {
          if (document.head) {
            observer.disconnect();
            if (qrStyle.isConnected) {
              document.head.append(qrStyle);
            }
          }
        });
        observer.observe(root, {
          childList: true
        });
      }

      // Collapse all quotes, when option is enabled
      console.debug("[QuoteColors] [quotecollapse_msgDisplay.js]: bPrefQuoteCollapseByDefault = " + this.bPrefQuoteCollapseByDefault);
      /*
      for (let quote of QuoteCollapse._getQuoteRoots(this.oMsgBody)) {
        QuoteCollapse._toggleFullyVisible(quote);
      }
      */
      // Use the following line instead of the lines above, to respect the collapse default setting
      QuoteCollapse._setSubTree(this.oMsgBody, !this.bPrefQuoteCollapseByDefault);
    }
  },

  _getQCPrefs : function()
  {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js]: QuoteCollapse._getQCPrefs");

    // ########################################################################
    // set value from user prefs
    this.bPrefEnableQuoteCollapse = options.enableQuoteCollapse;
    this.bPrefQuoteCollapseByDefault = options.quoteCollapseByDefault;
  },

  _toggleFullyVisible: function toggleFullyVisible(quote) {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_toggleFullyVisible]");

    if (quote.clientHeight < quote.scrollHeight)
      return false;

    for (let nested of QuoteCollapse._getQuoteRoots(quote)) {
      if (!toggleFullyVisible(nested))
        return false;
    }
    quote.setAttribute("qctoggled", "true");
    return true;
},

  _getState: function(node) {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_getState]");

    let current = node;
    while (current) {
      if (current.nodeName == "BLOCKQUOTE" && current.getAttribute(
          "qctoggled") != "true")
        return false;

      current = current.parentNode
    }
    return true;
  },

  _setState: function(node, state, bubble) {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_setState]");

    if (state)
      node.setAttribute("qctoggled", "true");
    else
      node.setAttribute("qctoggled", "false");

    if (bubble) {
      var currentParent = node.parentNode;
      while (currentParent) {
        if (currentParent.nodeName == 'BLOCKQUOTE')
          QuoteCollapse._setState(currentParent, state);

        currentParent = currentParent.parentNode;
      }
    }
  },

  _setSubTree: function(node, state) {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_setSubTree]");

    if (node.nodeName == 'BLOCKQUOTE')
      QuoteCollapse._setState(node, state);

    for (var i = 0; i < node.childNodes.length; i++) {
      QuoteCollapse._setSubTree(node.childNodes.item(i), state);
    }
  },

  _setSubTreeLevel: function(node, state, level) {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_setSubTreeLevel]");

    if (node.nodeName == 'BLOCKQUOTE') {
      if (level <= 0) {
        QuoteCollapse._setState(node, state, state);
        if (state)
          for (let nested of node.querySelectorAll("blockquote")) {
            QuoteCollapse._toggleFullyVisible(nested);
          }

        return; // no need to go deeper
      }
      level--; // only BQs count for the level magic
    }
    for (var i = 0; i < node.childNodes.length; i++) {
      QuoteCollapse._setSubTreeLevel(node.childNodes.item(i), state, level);
    }
  },

  // we could use subtree on BODY, but the following is more efficient
  _setTree: function(doc, newstate) {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_setTree] newstate = " + newstate);

    var tree = doc.getElementsByTagName("blockquote");
    for (var i = 0; i < tree.length; i++)
      QuoteCollapse._setState(tree.item(i), newstate);
  },

  _setLevel: function(target, newstate) {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_setLevel]");

    var level = 0;
    var node = target;
    do {
      node = node.parentNode;
      if (node.nodeName == 'BLOCKQUOTE')
        level++;
    } while (node.nodeName != 'BODY');
    QuoteCollapse._setSubTreeLevel(node, newstate,
    level); // node is the BODY element
  },

  // this is called by a click event
  _onClick: function(event) {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_onClick]");

    var target = event.target;
    if (target.nodeName == 'SPAN')
      target = target.parentNode; // cite-tags span?
    if (target.nodeName == 'PRE')
      target = target.parentNode; // PRE inside; don't walk all the way up
    if (target.nodeName != 'BLOCKQUOTE')
      return true;

    var newstate = !QuoteCollapse._getState(target);


    // react only to active spot (leave rest for copy etc.)
    if (event.pageX > target.offsetLeft + 12) return true;

    if (event.shiftKey)
      if (event.ctrlKey || event.metaKey)
        QuoteCollapse._setTree(target.ownerDocument, newstate);
      else
        QuoteCollapse._setSubTree(target, newstate);
    else
    if (event.ctrlKey || event.metaKey)
      QuoteCollapse._setLevel(target, newstate);
    else {
      QuoteCollapse._setState(target, newstate, newstate);
      if (newstate)
        for (let nested of target.querySelectorAll("blockquote")) {
          QuoteCollapse._toggleFullyVisible(nested);
        }
    }
    return true;
  },

  _getQuoteRoots: function getQuoteRoots(node, result = []) {
    console.debug("[QuoteColors] [quotecollapse_msgDisplay.js] [_getQuoteRoots]");

    for (let childElement of node.children) {
      if (childElement.localName == "blockquote")
        result.push(childElement);
      else
        getQuoteRoots(childElement, result);
    }
    return result;
  },

};

QuoteCollapse._onLoad();