const DefaultOptions = {
  colorText: true,
  colorBackground: true,

  /******* Only for reference: Thunderbirds own colors *****
      fg_l1: "#729FCF",
      fg_l2: "#AD7FA8",
      fg_l3: "#8AE234",
      fg_l4: "#FCAF3E",
      fg_l5: "#E9B96E",

      Original Darkmode Background: #181A1B
  **********************************************************/

  fg_l1: "#000080",
  fg_l2: "#800000",
  fg_l3: "#008000",
  fg_l4: "#800080",
  fg_l5: "#008080",
  bg_l1: "#F5F5F5",
  bg_l2: "#EBEBEB",
  bg_l3: "#E1E1E1",
  bg_l4: "#D7D7D7",
  bg_l5: "#CDCDCD",

  dm_fg_l1: "#46A3FF",
  dm_fg_l2: "#E80000",
  dm_fg_l3: "#00B000",
  dm_fg_l4: "#FF42FF",
  dm_fg_l5: "#00F9F9",
  dm_bg_l1: "#3A3A3F",
  dm_bg_l2: "#47474C",
  dm_bg_l3: "#535359",
  dm_bg_l4: "#5F5F66",
  dm_bg_l5: "#6B6B72",

  borderMode: 0,
  borderColor: "#000000",
  dm_borderColor: "#F9F9FA",

  borderStyle: 1,
  borderWidth: 1,
  borderposition_bottom: false,
  borderposition_left: true,
  borderposition_right: false,
  borderposition_top: false,
  collapseBorders: false,

  colorHTMLmessages: true,

  usermsgcolors: false,

  messagetextcolor: "#000000",
  messagebgcolor: "#FFFFFF",
  messagelinkcolor: "#0000EF",
  messagelinkhovercolor: "#0000BF",
  signaturecolor: "#808080",
  signaturelinkcolor: "#0000FF",

  dm_messagetextcolor: "#F9F9FA",
  dm_messagebgcolor: "#2A2A2E",
  dm_messagelinkcolor: "#F9F9FA",
  dm_messagelinkhovercolor: "#C5C5CF",
  dm_signaturecolor: "#808080",
  dm_signaturelinkcolor: "#808080",

  hidesignatures: false,
  hidestructdelimiters: true,

  enableQuotecolorsOnCompose: false,
  enableUsermsgcolorsOnCompose: false,

  enableQuoteCollapse: false,
  quoteCollapseByDefault: false
}
const OptionsList = Object.keys(DefaultOptions);

const LightmodeColorOptions = {
  fg_l1: DefaultOptions.fg_l1,
  fg_l2: DefaultOptions.fg_l2,
  fg_l3: DefaultOptions.fg_l3,
  fg_l4: DefaultOptions.fg_l4,
  fg_l5: DefaultOptions.fg_l5,
  bg_l1: DefaultOptions.bg_l1,
  bg_l2: DefaultOptions.bg_l2,
  bg_l3: DefaultOptions.bg_l3,
  bg_l4: DefaultOptions.bg_l4,
  bg_l5: DefaultOptions.bg_l5
}
const LightmodeColorOptionsList = Object.keys(LightmodeColorOptions);

const DarkmodeColorOptions = {
  dm_fg_l1: DefaultOptions.dm_fg_l1,
  dm_fg_l2: DefaultOptions.dm_fg_l2,
  dm_fg_l3: DefaultOptions.dm_fg_l3,
  dm_fg_l4: DefaultOptions.dm_fg_l4,
  dm_fg_l5: DefaultOptions.dm_fg_l5,
  dm_bg_l1: DefaultOptions.dm_bg_l1,
  dm_bg_l2: DefaultOptions.dm_bg_l2,
  dm_bg_l3: DefaultOptions.dm_bg_l3,
  dm_bg_l4: DefaultOptions.dm_bg_l4,
  dm_bg_l5: DefaultOptions.dm_bg_l5
}
const DarkmodeColorOptionsList = Object.keys(DarkmodeColorOptions);

function defaultError(error) {
  console.error("[QuoteColors]: Error:", error);
}