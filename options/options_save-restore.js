function saveOptions(e) {
  e.preventDefault();

/*
  let tabsize = Number(document.getElementById("tabsize").value);
  if (!Number.isInteger(tabsize) || tabsize <= 0)
    tabsize = DefaultOptions.tabsize;
*/

  return messenger.storage.local.set({
    colorText: document.getElementById("colorText").checked,
    colorBackground: document.getElementById("colorBackground").checked,

    fg_l1: document.getElementById("fg_l1").value,
    fg_l2: document.getElementById("fg_l2").value,
    fg_l3: document.getElementById("fg_l3").value,
    fg_l4: document.getElementById("fg_l4").value,
    fg_l5: document.getElementById("fg_l5").value,
    bg_l1: document.getElementById("bg_l1").value,
    bg_l2: document.getElementById("bg_l2").value,
    bg_l3: document.getElementById("bg_l3").value,
    bg_l4: document.getElementById("bg_l4").value,
    bg_l5: document.getElementById("bg_l5").value,

    dm_fg_l1: document.getElementById("dm_fg_l1").value,
    dm_fg_l2: document.getElementById("dm_fg_l2").value,
    dm_fg_l3: document.getElementById("dm_fg_l3").value,
    dm_fg_l4: document.getElementById("dm_fg_l4").value,
    dm_fg_l5: document.getElementById("dm_fg_l5").value,
    dm_bg_l1: document.getElementById("dm_bg_l1").value,
    dm_bg_l2: document.getElementById("dm_bg_l2").value,
    dm_bg_l3: document.getElementById("dm_bg_l3").value,
    dm_bg_l4: document.getElementById("dm_bg_l4").value,
    dm_bg_l5: document.getElementById("dm_bg_l5").value,

    borderMode: document.querySelector('input[name=borderMode]:checked').value,
    borderColor: document.getElementById("borderColor").value,
    dm_borderColor: document.getElementById("dm_borderColor").value,

    borderStyle: document.getElementById("borderStyle").value,
    borderWidth: document.getElementById("borderWidth").value,
    borderposition_bottom: document.getElementById("borderposition_bottom").checked,
    borderposition_left: document.getElementById("borderposition_left").checked,
    borderposition_right: document.getElementById("borderposition_right").checked,
    borderposition_top: document.getElementById("borderposition_top").checked,
    collapseBorders: document.getElementById("collapseBorders").checked,

    colorHTMLmessages: document.getElementById("colorHTMLmessages").checked,

    usermsgcolors: document.getElementById("usermsgcolors").checked,

    messagetextcolor: document.getElementById("messagetextcolor").value,
    messagebgcolor: document.getElementById("messagebgcolor").value,
    messagelinkcolor: document.getElementById("messagelinkcolor").value,
    messagelinkhovercolor: document.getElementById("messagelinkhovercolor").value,
    signaturecolor: document.getElementById("signaturecolor").value,
    signaturelinkcolor: document.getElementById("signaturelinkcolor").value,

    dm_messagetextcolor: document.getElementById("dm_messagetextcolor").value,
    dm_messagebgcolor: document.getElementById("dm_messagebgcolor").value,
    dm_messagelinkcolor: document.getElementById("dm_messagelinkcolor").value,
    dm_messagelinkhovercolor: document.getElementById("dm_messagelinkhovercolor").value,
    dm_signaturecolor: document.getElementById("dm_signaturecolor").value,
    dm_signaturelinkcolor: document.getElementById("dm_signaturelinkcolor").value,

    hidesignatures: document.getElementById("hidesignatures").checked,
    hidestructdelimiters: document.getElementById("hidestructdelimiters").checked,

    enableQuotecolorsOnCompose: document.getElementById("enableQuotecolorsOnCompose").checked,
    enableUsermsgcolorsOnCompose: document.getElementById("enableUsermsgcolorsOnCompose").checked,

    enableQuoteCollapse: document.getElementById("enableQuoteCollapse").checked,
    quoteCollapseByDefault: document.getElementById("quoteCollapseByDefault").checked
  });
}

function restoreOption(id) {
  return messenger.storage.local.get(id).then((res) => {
    console.debug("ID: " + id);
    if (id == "borderMode") {
      let elements = document.getElementsByName('borderMode');
      let num = res[id] || DefaultOptions[id];
      elements[num].checked = true;
    } else {
      let element = document.getElementById(id);
      if (element.type && element.type == "checkbox") {
        console.debug( "[QuoteColors] [restoreOption]: " + element.id + ": " + res[id] );
        if (res[id] === undefined) {
          element.checked = DefaultOptions[id];
        } else {
          element.checked = res[id];
        }
      }
      else {
        element.value = res[id] || DefaultOptions[id];
      }
    }
  }, defaultError);
}

async function resetLightmodeColorOptions() {
  messenger.storage.local.remove(LightmodeColorOptionsList).then(() => {
    restoreAllOptions();
  });
}
async function resetDarkmodeColorOptions() {
  messenger.storage.local.remove(DarkmodeColorOptionsList).then(() => {
    restoreAllOptions();
  });
}

async function restoreAllOptions() {
  /*
    OptionsList.forEach((option) => {
      await restoreOption(option);
    });
  */

  await restoreOption("colorText");
  await restoreOption("colorBackground");

  await restoreOption("fg_l1");
  await restoreOption("fg_l2");
  await restoreOption("fg_l3");
  await restoreOption("fg_l4");
  await restoreOption("fg_l5");
  await restoreOption("bg_l1");
  await restoreOption("bg_l2");
  await restoreOption("bg_l3");
  await restoreOption("bg_l4");
  await restoreOption("bg_l5");

  await restoreOption("dm_fg_l1");
  await restoreOption("dm_fg_l2");
  await restoreOption("dm_fg_l3");
  await restoreOption("dm_fg_l4");
  await restoreOption("dm_fg_l5");
  await restoreOption("dm_bg_l1");
  await restoreOption("dm_bg_l2");
  await restoreOption("dm_bg_l3");
  await restoreOption("dm_bg_l4");
  await restoreOption("dm_bg_l5");

  await restoreOption("borderMode");
  await restoreOption("borderColor");
  await restoreOption("dm_borderColor");

  await restoreOption("borderStyle");
  await restoreOption("borderWidth");
  await restoreOption("borderposition_bottom");
  await restoreOption("borderposition_left");
  await restoreOption("borderposition_right");
  await restoreOption("borderposition_top");
  await restoreOption("collapseBorders");

  await restoreOption("colorHTMLmessages");

  await restoreOption("usermsgcolors");

  await restoreOption("messagetextcolor");
  await restoreOption("messagebgcolor");
  await restoreOption("messagelinkcolor");
  await restoreOption("messagelinkhovercolor");
  await restoreOption("signaturecolor");
  await restoreOption("signaturelinkcolor");

  await restoreOption("dm_messagetextcolor");
  await restoreOption("dm_messagebgcolor");
  await restoreOption("dm_messagelinkcolor");
  await restoreOption("dm_messagelinkhovercolor");
  await restoreOption("dm_signaturecolor");
  await restoreOption("dm_signaturelinkcolor");

  await restoreOption("hidesignatures");
  await restoreOption("hidestructdelimiters");

  await restoreOption("enableQuotecolorsOnCompose");
  await restoreOption("enableUsermsgcolorsOnCompose");

  await restoreOption("enableQuoteCollapse");
  await restoreOption("quoteCollapseByDefault");
}

function resetAllOptions() {
  return messenger.storage.local.remove(OptionsList).then(() => {
    restoreAllOptions();
  });
}
