var parentElements = [
  "enableQuoteCollapse",
  "bordermode0",
  "bordermode1",
  "usermsgcolors",
];

// onLoad listener to build the UI and Preview
document.addEventListener('DOMContentLoaded', () => {
  console.debug("[QuoteColors] [options_listeners.js]: DOMContentLoaded, restoreAllOptions()");
  restoreAllOptions()
  .then(() => { checkThinDouble(); })
  .then(() => { setDisabledAttribute(parentElements); })
  .then(() => { updatePreview(); });
});

// onChange listener for all options to save the changed options
OptionsList.forEach((option) => {
  console.debug("option: " + option);
  if (option == "borderMode") {
    document.getElementById("bordermode0").addEventListener("change", (e) => {
      console.debug("[QuoteColors] [options_listeners.js]: Option borderMode-0 changed, saveOptions()");
      saveOptions(e)
      .then(() => { updatePreview(); });
    });
    document.getElementById("bordermode1").addEventListener("change", (e) => {
      console.debug("[QuoteColors] [options_listeners.js]: Option borderMode-1 changed, saveOptions()");
      saveOptions(e)
      .then(() => { updatePreview(); });
    });
} else {
    document.getElementById(option).addEventListener("change", (e) => {
      console.debug("[QuoteColors] [options_listeners.js]: Other Option changed, saveOptions()");
      saveOptions(e)
      .then(() => { updatePreview(); });
    });
  }
});

// reset click listener
document.getElementById("reset").addEventListener("click", () => {
  console.debug("[QuoteColors] [options_listeners.js]: Reset clicked, resetAllOptions()");
  resetAllOptions()
  .then(() => { waitAndUpdatePreview(); });
});

// reset click light mode colors listener
document.getElementById("reset_colors").addEventListener("click", () => {
  console.debug("[QuoteColors] [options_listeners.js]: Reset Light-Mode Colors clicked, resetLightmodeColorOptions()");
  resetLightmodeColorOptions()
  .then(() => { waitAndUpdatePreview(); });
});

// reset dark mode colors click listener
document.getElementById("dm_reset_colors").addEventListener("click", () => {
  console.debug("[QuoteColors] [options_listeners.js]: Reset Dark-Mode Colors clicked, resetDarkmodeColorOptions()");
  resetDarkmodeColorOptions()
  .then(() => { waitAndUpdatePreview(); });
});

// check Thin versus Double borders listener
document.getElementById("borderWidth").addEventListener("change", (e) => {
  checkThinDouble();
});
document.getElementById("borderStyle").addEventListener("change", (e) => {
  checkThinDouble();
});

// onChange listeners to invoke setAttributeDisabled for nested options
parentElements.forEach((parentElement) => {
  console.debug("parentElement: " + parentElement);
  document.getElementById(parentElement).addEventListener("change", (e) => {
    setDisabledAttribute([parentElement]);
  });
});
